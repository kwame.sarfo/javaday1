import java.util.Random;

public class ModifiedRandomInteger {
    public static void main(String[] args) {
        Random randomInteger = new Random();
        int value = randomInteger.nextInt(0, 100);

        if (value == 0)
        {
            System.out.println("number " + value + " is FROZEN");
        }
        else if (value <= 14 && value >= 1)
        {
            System.out.println("number " + value + " is COLD");
        }
        else if (value <= 24 && value >= 15)
        {
            System.out.println("number " + value + " is COOL");
        }
        else if (value <= 40 && value >= 25)
        {
            System.out.println("number " + value + " is WARM");
        }
        else if (value <= 60 && value >= 41)
        {
            System.out.println("number " + value + " is HOT");
        }
        else if (value <= 80 && value >= 61)
        {
            System.out.println("number " + value + " is VERY HOT");
        }
        else if (value <= 99 && value >= 81)
        {
            System.out.println("number " + value + " is EXTREMELY HOT");
        }
        else
        {
            System.out.println("number " + value + " is BOILING");
        }

    }
}
