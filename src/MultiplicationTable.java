public class MultiplicationTable {
    public static void main(String[] args) {
        int multiplier = 1;
        int num = 1;

        while(num <= 10)
        {
            if(multiplier <= 12)
            {
                System.out.println(num + "*" + multiplier + " = " + num*multiplier );
                multiplier++;
                continue;
            }
            num++;
            multiplier = 1;
        }
    }
}
