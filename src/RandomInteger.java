import java.util.Random;

public class RandomInteger {
    public static void main(String[] args) {
        Random randomInteger = new Random();
        int value = randomInteger.nextInt(0, 100);

        if(value % 2 == 0)
        {
            System.out.println("Number " + value + " is even");
        }
        else
        {
            System.out.println("Number " + value + " is odd");
        }
    }
}
