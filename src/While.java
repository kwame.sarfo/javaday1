import java.util.Random;

public class While {
    public static void main(String[] args) {
        Random randomInt = new Random();
        boolean checker = true;

        while(checker)
        {
            int value = randomInt.nextInt(-3,3);
            if (value != 0) {
                System.out.println(value);
                value = randomInt.nextInt();
            }
            else
            {
                checker = false;
            }
        }
    }
}
